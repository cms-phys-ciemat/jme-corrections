import os
import envyaml

from Base.Modules.baseModules import JetLepMetSyst
from analysis_tools.utils import import_root
ROOT = import_root()

import correctionlib
correctionlib.register_pyroot_binding()

corrCfg = envyaml.EnvYAML('%s/src/Corrections/JME/python/jetCorrectionsFiles.yaml' % 
                                    os.environ['CMSSW_BASE'])

class jecRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(jecRDFProducer, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year") ; year = str(self.year)
        self.runPeriod = kwargs.pop("runPeriod")
        self.type = kwargs.pop("type")
        self.skip_unused_systs = kwargs.pop("skipUnusedSysts", False)

        self.LUT = self.type+"_"+("mc" if self.isMC else "data")+str(self.year)+self.runPeriod

        prefix = "" ; isUL = False
        try:
            isUL = kwargs.pop("isUL")
            prefix += "" if not isUL else "UL"
        except KeyError:
            pass
        try:
            prefix += self.runPeriod
        except KeyError:
            pass

        self.corrKey = prefix+year

        if self.year < 2022:
            raise ValueError("Only implemented for Run3 datasets")

        if not os.getenv("_jerccorr"):
            os.environ["_jerccorr"] = "_jerccorr"

            if "/libCorrectionsJME.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libCorrectionsJME.so")
            base = "{}/src/Corrections/JME".format(os.getenv("CMSSW_BASE"))
            ROOT.gROOT.ProcessLine(".L {}/interface/jetCorrectionsInterface.h".format(base))

        if not os.getenv(self.LUT+"_jeccorr"):
            os.environ[self.LUT+"_jeccorr"] = self.LUT+"_jeccorr"
            # this if/else initializes the central LUTs
            if self.isMC:
                L1 = corrCfg[self.type][self.corrKey]["corrNameMC"]+corrCfg["tagsMC"]["L1"]+corrCfg[self.type][self.corrKey]["AK"]
                L2 = corrCfg[self.type][self.corrKey]["corrNameMC"]+corrCfg["tagsMC"]["L2"]+corrCfg[self.type][self.corrKey]["AK"]
                L3 = corrCfg[self.type][self.corrKey]["corrNameMC"]+corrCfg["tagsMC"]["L3"]+corrCfg[self.type][self.corrKey]["AK"]

                ROOT.gInterpreter.Declare("""
                    auto %s = jetCorrections("%s", "%s", "%s", "%s");
                """ % (self.LUT,
                       corrCfg[self.type][self.corrKey]["fileName"],
                       L1, L2, L3))
            else:
                L1   = corrCfg[self.type][self.corrKey]["corrNameMC"]+corrCfg["tagsDATA"]["L1"]+corrCfg[self.type][self.corrKey]["AK"]
                L2   = corrCfg[self.type][self.corrKey]["corrNameMC"]+corrCfg["tagsDATA"]["L2"]+corrCfg[self.type][self.corrKey]["AK"]
                L3   = corrCfg[self.type][self.corrKey]["corrNameMC"]+corrCfg["tagsDATA"]["L3"]+corrCfg[self.type][self.corrKey]["AK"]
                L2L3 = corrCfg[self.type][self.corrKey]["corrNameMC"]+corrCfg["tagsDATA"]["L2L3"]+corrCfg[self.type][self.corrKey]["AK"]

                ROOT.gInterpreter.Declare("""
                    auto %s = jetCorrections("%s", "%s", "%s", "%s", "%s");
                """ % (self.LUT,
                       corrCfg[self.type][self.corrKey]["fileName"],
                       L1, L2, L3, L2L3))

            # these if initializes the JES systematics LUTs
            if self.isMC:
                for jes_syst in corrCfg["jes_systematics"]:
                    # skip all the cases for which the systematic variation would not actually be used
                    if self.skip_unused_systs and jes_syst not in self.jet_syst:
                        continue

                    LSYST = corrCfg[self.type][self.corrKey]["corrNameMC"]+jes_syst+corrCfg[self.type][self.corrKey]["AK"]

                    ROOT.gInterpreter.Declare("""
                        auto %s = jetCorrections("%s", "%s");
                    """ % (self.LUT+jes_syst,
                           corrCfg[self.type][self.corrKey]["fileName"],
                           LSYST))

    def run(self, df):
        branches = []

        if self.type == "jerc":
            if self.year == 2023 and self.runPeriod == "postBPix":
                df = df.Define("jec_sf", 
                               """%s.get_jec_sf(Jet_area, Jet_eta, Jet_phi, Jet_pt,
                                    Rho_fixedGridRhoFastjetAll, Jet_rawFactor)""" % self.LUT)
            else:
                df = df.Define("jec_sf", 
                               """%s.get_jec_sf(Jet_area, Jet_eta, Jet_pt,
                                    Rho_fixedGridRhoFastjetAll, Jet_rawFactor)""" % self.LUT)

            df = df.Define("Jet_pt_corr", 
                           "Jet_pt * (1 - Jet_rawFactor) * jec_sf")

            df = df.Define("Jet_mass_corr", 
                           "Jet_mass * (1 - Jet_rawFactor) * jec_sf")

            branches.append("jec_sf")
            branches.append("Jet_pt_corr")
            branches.append("Jet_mass_corr")

            if self.isMC:
                for jes_syst in corrCfg["jes_systematics"]:
                    # skip all the cases for which the systematic variation would not actually be used
                    if self.skip_unused_systs and jes_syst not in self.jet_syst:
                        continue

                    for drctn in ["_up", "_down"]:
                        # skip all the cases for which the systematic variation would not actually be used
                        if self.skip_unused_systs and drctn not in self.jet_syst:
                            continue

                        df = df.Define("jec_sf%s%s" % (jes_syst, drctn), 
                                """%s.get_jec_sf(Jet_eta, Jet_pt, Jet_rawFactor,
                                                jec_sf, "%s")""" % (self.LUT+jes_syst, drctn))

                        df = df.Define("Jet_pt_corr%s%s" % (jes_syst, drctn),
                            "Jet_pt * (1 - Jet_rawFactor) * jec_sf%s%s" % (jes_syst, drctn))

                        df = df.Define("Jet_mass_corr%s%s" % (jes_syst, drctn),
                            "Jet_mass * (1 - Jet_rawFactor) * jec_sf%s%s" % (jes_syst, drctn))

                        branches.append("jec_sf%s%s" % (jes_syst, drctn))
                        branches.append("Jet_pt_corr%s%s" % (jes_syst, drctn))
                        branches.append("Jet_mass_corr%s%s" % (jes_syst, drctn))

        elif self.type == "fatjerc":
            if self.year == 2023 and self.runPeriod == "postBPix":
                df = df.Define("fatjec_sf", 
                               """%s.get_jec_sf(FatJet_area, FatJet_eta, FatJet_phi, FatJet_pt,
                                    Rho_fixedGridRhoFastjetAll, FatJet_rawFactor)""" % self.LUT)
            else:
                df = df.Define("fatjec_sf", 
                               """%s.get_jec_sf(FatJet_area, FatJet_eta, FatJet_pt,
                                    Rho_fixedGridRhoFastjetAll, FatJet_rawFactor)""" % self.LUT)

            df = df.Define("FatJet_pt_corr", 
                           "FatJet_pt * (1 - FatJet_rawFactor) * fatjec_sf")

            df = df.Define("FatJet_mass_corr", 
                           "FatJet_mass * (1 - FatJet_rawFactor) * fatjec_sf")

            df = df.Define("FatJet_msoftdrop_corr",
                           "FatJet_msoftdrop * (1 - FatJet_rawFactor) * fatjec_sf")

            branches.append("fatjec_sf")
            branches.append("FatJet_pt_corr")
            branches.append("FatJet_mass_corr")
            branches.append("FatJet_msoftdrop_corr")

            if self.isMC:
                for jes_syst in corrCfg["jes_systematics"]:
                    # skip all the cases for which the systematic variation would not actually be used
                    if self.skip_unused_systs and jes_syst not in self.jet_syst:
                        continue

                    for drctn in ["_up", "_down"]:
                        # skip all the cases for which the systematic variation would not actually be used
                        if self.skip_unused_systs and drctn not in self.jet_syst:
                            continue

                        df = df.Define("fatjec_sf%s%s" % (jes_syst, drctn), 
                                """%s.get_jec_sf(FatJet_eta, FatJet_pt, FatJet_rawFactor,
                                                fatjec_sf, "%s")""" % (self.LUT+jes_syst, drctn))

                        df = df.Define("FatJet_pt_corr%s%s" % (jes_syst, drctn),
                            "FatJet_pt * (1 - FatJet_rawFactor) * fatjec_sf%s%s" % (jes_syst, drctn))

                        df = df.Define("FatJet_mass_corr%s%s" % (jes_syst, drctn),
                            "FatJet_mass * (1 - FatJet_rawFactor) * fatjec_sf%s%s" % (jes_syst, drctn))

                        df = df.Define("FatJet_msoftdrop_corr%s%s" % (jes_syst, drctn),
                            "FatJet_msoftdrop * (1 - FatJet_rawFactor) * fatjec_sf%s%s" % (jes_syst, drctn))

                        branches.append("fatjec_sf%s%s" % (jes_syst, drctn))
                        branches.append("FatJet_pt_corr%s%s" % (jes_syst, drctn))
                        branches.append("FatJet_mass_corr%s%s" % (jes_syst, drctn))
                        branches.append("FatJet_msoftdrop_corr%s%s" % (jes_syst, drctn))

        else:
            raise ValueError("Only jerc/fatjerc available")

        return df, branches

def jecRDF(**kwargs):
    """
    Module to re-apply JEC corrections to AK4 and AK8 jets.
    JEC corrections are by default already included in Jet_* variables in NanoAOD but
    JME POG always suggests to re-apply them at analysis level in order to have the 
    latest and greatest conditions included.

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: jecRDF
            path: Base.Modules.jmeCorrections
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                runPeriod: self.config.runPeriod
                type: jerc/fatjerc
                skip_unused_systs: True/False (default False)
    """

    return lambda: jecRDFProducer(**kwargs)


class jerRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(jerRDFProducer, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year") ; year = str(self.year)
        self.runPeriod = kwargs.pop("runPeriod")
        self.type = kwargs.pop("type")
        self.after_jec = kwargs.pop("after_jec", False)
        self.synchedRNG = kwargs.pop("synchedRNG", False)
        self.skip_unused_systs = kwargs.pop("skipUnusedSysts", False)

        self.LUT = self.type[:-1]+"_"+("mc" if self.isMC else "data")+str(self.year)+self.runPeriod

        prefix = "" ; isUL = False
        try:
            isUL = kwargs.pop("isUL")
            prefix += "" if not isUL else "UL"
        except KeyError:
            pass
        try:
            prefix += self.runPeriod
        except KeyError:
            pass

        self.corrKey = prefix+year

        if self.year < 2022:
            raise ValueError("Only implemented for Run3 datasets")

        if not os.getenv("_jerccorr"):
            os.environ["_jerccorr"] = "_jerccorr"

            if "/libCorrectionsJME.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libCorrectionsJME.so")
            base = "{}/src/Corrections/JME".format(os.getenv("CMSSW_BASE"))
            ROOT.gROOT.ProcessLine(".L {}/interface/jetCorrectionsInterface.h".format(base))

        if not os.getenv(self.LUT+"_jercorr"):
            os.environ[self.LUT+"_jercorr"] = self.LUT+"_jercorr"

            if self.isMC:
                corrname_ptRes = corrCfg[self.type][self.corrKey]["corrNamePtRes"]+corrCfg[self.type][self.corrKey]["AK"]
                corrname_smearSf = corrCfg[self.type][self.corrKey]["corrNameSf"]+corrCfg[self.type][self.corrKey]["AK"]

                ROOT.gInterpreter.Declare("""
                    auto %s = jetCorrections("%s", "%s", "%s");
                """ % (self.LUT,
                       corrCfg[self.type][self.corrKey]["fileName"],
                       corrname_ptRes, corrname_smearSf))

    def run(self, df):
        if not self.isMC:
            return df, []
        all_branches = df.GetColumnNames()

        # Run-2 uses fixedGridRhoFastjetAll and Run-3 Rho_fixedGridRhoFastjetAll
        rho = "fixedGridRhoFastjetAll"
        if rho not in all_branches:
            rho = "Rho_fixedGridRhoFastjetAll"

        branches = ["jet_smear_seed", "jet_smear_factor", "jet_smear_factor_down",
                    "jet_smear_factor_up", "jet_pt_resolution"]

        pt = "Jet_pt"
        mass = "Jet_mass"
        eta = "Jet_eta"
        phi = "Jet_phi"
        jertype = "jer"
        jetsize = ""
        fatPrefix = ""
        if self.type == "fatjerc":
            pt   = "Fat" + pt
            mass = "Fat" + mass
            eta  = "Fat" + eta
            phi  = "Fat" + phi
            jertype = "fatjer"
            jetsize = "AK8"
            fatPrefix  = "fat"

        if self.after_jec:
            pt += "_corr"
            mass += "_corr"

        if self.synchedRNG:
            df = df.Define(f"__{jertype}_results", f"{self.LUT}.get_jer_outputs("
                f"event_seed, {pt}, {eta}, {phi}, {mass}, "
                f"GenJet{jetsize}_pt, GenJet{jetsize}_eta, GenJet{jetsize}_phi, "
                f"GenJet{jetsize}_mass, {rho})")

        else:
            df = df.Define(f"__{jertype}_results", f"{self.LUT}.get_jer_outputs("
                f"run, luminosityBlock, event, {pt}, {eta}, {phi}, {mass}, "
                f"GenJet{jetsize}_pt, GenJet{jetsize}_eta, GenJet{jetsize}_phi, "
                f"GenJet{jetsize}_mass, {rho})")

        branches2store = []
        for ib, branch in enumerate(branches):
            # skip all the cases for which the systematic variation would not actually be used
            if self.skip_unused_systs:
                if not "smeared" in self.jet_syst:
                    if "_up" in branch or "_down" in branch:
                        continue
                else:
                    if "_up" in branch and not "_up" in self.jet_syst:
                        continue
                    if "_down" in branch and not "_down" in self.jet_syst:
                        continue

            df = df.Define(fatPrefix+branch, f"__{jertype}_results.{branch}")
            branches2store.append(fatPrefix+branch)

        return df, branches2store

def jerRDF(**kwargs):
    """
    Module to re-apply JER corrections to AK4 and AK8 jets.
    JER corrections are by default already included in Jet_* variables in NanoAOD but
    JME POG always suggests to re-apply them at analysis level in order to have the 
    latest and greatest conditions included.

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: jerRDF
            path: Base.Modules.jmeCorrections
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                runPeriod: self.config.runPeriod
                type: jerc/fatjerc
                after_jec: True/False (default False)
                synchedRNG: True/False (default False)
                skip_unused_systs: True/False (default False)
    """

    return lambda: jerRDFProducer(**kwargs)


class metcRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(metcRDFProducer, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year")
        self.toPropagate = kwargs.pop("toPropagate", [])
        self.skip_unused_systs = kwargs.pop("skipUnusedSysts", False)

        if ("JES" not in self.toPropagate and "JER" not in self.toPropagate
            and "TES" not in self.toPropagate and "ESS" not in self.toPropagate):
            raise ValueError("Please specify which correction to propagate for the PuppiMET.")

        # remove JER and TES from propagation in Data because they do not exist there
        if not self.isMC:
            if "JER" in self.toPropagate: self.toPropagate.remove("JER")
            if "TES" in self.toPropagate: self.toPropagate.remove("TES")

        if not os.getenv("_METShift"):
            os.environ["_METShift"] = "METShift"

            if "/libCorrectionsJME.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libCorrectionsJME.so")
            base = "{}/src/Corrections/JME".format(os.getenv("CMSSW_BASE"))
            ROOT.gROOT.ProcessLine(".L {}/interface/metShift.h".format(base))

            ROOT.gInterpreter.Declare('auto met_shifter = metShift();')

    def run(self, df):
        branches = []

        if "JES" in self.toPropagate:
            # computing the central value shift of the JES corrected MET
            df = df.Define("MET_JESpropagated", "met_shifter.get_shifted_met("
                          f"Jet_eta, Jet_phi, Jet_pt, Jet_mass, "
                          f"Jet_pt_corr, Jet_mass_corr, "
                          "PuppiMET_pt, PuppiMET_phi)")

            # computing the systematics shift of the JES corrected MET
            # skip all the cases for which the systematic variation would not actually be used
            if self.isMC and self.jet_syst != self.jet_central and "smeared" not in self.jet_syst:
                df = df.Define(f"MET_JESpropagated{self.jet_syst}",
                    f"met_shifter.get_shifted_met("
                    f"Jet_eta, Jet_phi, Jet_pt, Jet_mass, "
                    f"Jet_pt{self.jet_syst}, Jet_mass{self.jet_syst}, "
                    "PuppiMET_pt, PuppiMET_phi, 15.0)")

                df = df.Define(f"PuppiMET_smeared_pt{self.jet_syst}",
                               f"MET_JESpropagated{self.jet_syst}[0]")
                df = df.Define(f"PuppiMET_smeared_phi{self.jet_syst}",
                               f"MET_JESpropagated{self.jet_syst}[1]")
                branches.append(f"PuppiMET_smeared_pt{self.jet_syst}")
                branches.append(f"PuppiMET_smeared_phi{self.jet_syst}")

        if "JER" in self.toPropagate:
            # if/else computing the central value of the JER corrected MET
            if "JES" in self.toPropagate:
                df = df.Define("MET_JERpropagated", "met_shifter.get_shifted_met("
                              f"Jet_eta, Jet_phi, Jet_pt_corr, Jet_mass_corr, "
                              f"Jet_pt{self.jet_central}, Jet_mass{self.jet_central}, "
                              "MET_JESpropagated[0], MET_JESpropagated[1])")
            else:
                df = df.Define("MET_JERpropagated", "met_shifter.get_shifted_met("
                              f"Jet_eta, Jet_phi, Jet_pt, Jet_mass, "
                              f"Jet_pt{self.jet_central}, Jet_mass{self.jet_central}, "
                              "PuppiMET_pt, PuppiMET_phi)")

            # computing the systematics shift of the JER corrected MET
            # skip all the cases for which the systematic variation would not actually be used
            if self.jet_syst != self.jet_central and "smeared" in self.jet_syst:
                df = df.Define(f"MET_JERpropagated{self.jet_syst}",
                              f"met_shifter.get_shifted_met("
                              f"Jet_eta, Jet_phi, Jet_pt{self.jet_central}, Jet_mass{self.jet_central}, "
                              f"Jet_pt{self.jet_syst}, Jet_mass{self.jet_syst}, "
                              "MET_JERpropagated[0], MET_JERpropagated[1])")

                df = df.Define(f"PuppiMET_smeared_pt{self.jet_syst}",
                               f"MET_JERpropagated{self.jet_syst}[0]")
                df = df.Define(f"PuppiMET_smeared_phi{self.jet_syst}",
                               f"MET_JERpropagated{self.jet_syst}[1]")
                branches.append(f"PuppiMET_smeared_pt{self.jet_syst}")
                branches.append(f"PuppiMET_smeared_phi{self.jet_syst}")

        if "TES" in self.toPropagate:
            # if/elif/else computing the central value of the TES corrected MET
            if "JER" in self.toPropagate:
                df = df.Define("MET_TESpropagated", "met_shifter.get_shifted_met("
                              f"Tau_eta, Tau_phi, Tau_pt, Tau_mass, "
                              f"Tau_pt{self.tau_central}, Tau_mass{self.tau_central}, "
                              "MET_JERpropagated[0], MET_JERpropagated[1])")
            elif "JES" in self.toPropagate:
                df = df.Define("MET_TESpropagated", "met_shifter.get_shifted_met("
                              f"Tau_eta, Tau_phi, Tau_pt, Tau_mass, "
                              f"Tau_pt{self.tau_central}, Tau_mass{self.tau_central}, "
                              "MET_JESpropagated[0], MET_JESpropagated[1])")
            else:
                df = df.Define("MET_TESpropagated", "met_shifter.get_shifted_met("
                              f"Tau_eta, Tau_phi, Tau_pt, Tau_mass, "
                              f"Tau_pt{self.tau_central}, Tau_mass{self.tau_central}, "
                              "PuppiMET_pt, PuppiMET_phi)")

            # computing the systematics shift of the TES corrected MET
            # skip all the cases for which the systematic variation would not actually be used
            if self.tau_syst != self.tau_central:
                df = df.Define(f"MET_TESpropagated{self.tau_syst}",
                              f"met_shifter.get_shifted_met("
                              f"Tau_eta, Tau_phi, Tau_pt{self.tau_central}, Tau_mass{self.tau_central}, "
                              f"Tau_pt{self.tau_syst}, Tau_mass{self.tau_syst}, "
                              "MET_TESpropagated[0], MET_TESpropagated[1])")

                df = df.Define(f"PuppiMET_smeared_pt{self.tau_syst}",
                               f"MET_TESpropagated{self.tau_syst}[0]")
                df = df.Define(f"PuppiMET_smeared_phi{self.tau_syst}",
                               f"MET_TESpropagated{self.tau_syst}[1]")
                branches.append(f"PuppiMET_smeared_pt{self.tau_syst}")
                branches.append(f"PuppiMET_smeared_phi{self.tau_syst}")

        if "ESS" in self.toPropagate:
            # if/elif/elif/else computing the central value of the ESS corrected MET
            if "TES" in self.toPropagate:
                df = df.Define("MET_ESSpropagated", "met_shifter.get_shifted_met("
                              f"Electron_eta, Electron_phi, Electron_pt, Electron_mass, "
                              f"Electron_pt{self.electron_central}, Electron_mass{self.electron_central}, "
                              "MET_TESpropagated[0], MET_TESpropagated[1])")
            elif "JER" in self.toPropagate:
                df = df.Define("MET_ESSpropagated", "met_shifter.get_shifted_met("
                              f"Electron_eta, Electron_phi, Electron_pt, Electron_mass, "
                              f"Electron_pt{self.electron_central}, Electron_mass{self.electron_central}, "
                              "MET_JERpropagated[0], MET_JERpropagated[1])")
            elif "JES" in self.toPropagate:
                df = df.Define("MET_ESSpropagated", "met_shifter.get_shifted_met("
                              f"Electron_eta, Electron_phi, Electron_pt, Electron_mass, "
                              f"Electron_pt{self.electron_central}, Electron_mass{self.electron_central}, "
                              "MET_JESpropagated[0], MET_JESpropagated[1])")
            else:
                df = df.Define("MET_ESSpropagated", "met_shifter.get_shifted_met("
                              f"Electron_eta, Electron_phi, Electron_pt, Electron_mass, "
                              f"Electron_pt{self.electron_central}, Electron_mass{self.electron_central}, "
                              "PuppiMET_pt, PuppiMET_phi)")

            # computing the systematics shift of the ESS corrected MET
            # skip all the cases for which the systematic variation would not actually be used
            if self.isMC and self.electron_syst != self.electron_central:
                df = df.Define(f"MET_ESSpropagated{self.electron_syst}",
                              f"met_shifter.get_shifted_met("
                              f"Electron_eta, Electron_phi, Electron_pt{self.electron_central}, Electron_mass{self.electron_central}, "
                              f"Electron_pt{self.electron_syst}, Electron_mass{self.electron_syst}, "
                              "MET_ESSpropagated[0], MET_ESSpropagated[1])")
                df = df.Define(f"PuppiMET_smeared_pt{self.electron_syst}",
                               f"MET_ESSpropagated{self.electron_syst}[0]")
                df = df.Define(f"PuppiMET_smeared_phi{self.electron_syst}",
                               f"MET_ESSpropagated{self.electron_syst}[1]")
                branches.append(f"PuppiMET_smeared_pt{self.electron_syst}")
                branches.append(f"PuppiMET_smeared_phi{self.electron_syst}")

        # run the if/elif in inverse order of how the corrections are computed: in this way we are 
        # guaranteed to always get the most updated correction with all requested propagations
        if "ESS" in self.toPropagate:
            df = df.Define("PuppiMET_smeared_pt", "MET_ESSpropagated[0]")
            df = df.Define("PuppiMET_smeared_phi", "MET_ESSpropagated[1]")
        elif "TES" in self.toPropagate:
            df = df.Define("PuppiMET_smeared_pt", "MET_TESpropagated[0]")
            df = df.Define("PuppiMET_smeared_phi", "MET_TESpropagated[1]")
        elif "JER" in self.toPropagate:
            df = df.Define("PuppiMET_smeared_pt", "MET_JERpropagated[0]")
            df = df.Define("PuppiMET_smeared_phi", "MET_JERpropagated[1]")
        elif "JES" in self.toPropagate:
            df = df.Define("PuppiMET_smeared_pt", "MET_JESpropagated[0]")
            df = df.Define("PuppiMET_smeared_phi", "MET_JESpropagated[1]")
        else:
            raise ValueError("Something went wrong, the requested cental value for "
                             "the corrected MET was not computed.")

        branches.append("PuppiMET_smeared_pt")
        branches.append("PuppiMET_smeared_phi")

        return df, branches

def metcRDF(**kwargs):
    """
    Module to propagate JES, JER, and TES corrections to the MET.
    
    Type-1 corrections are by default already included in MET_* variables in NanoAOD but
    JME POG always suggests to re-apply them at analysis level in order to have the 
    latest and greatest conditions included.
    
    MET smearing is not included in MET_* variables in NanoAOD and must be applied
    at analysis level

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: metcRDF
            path: Base.Modules.jmeCorrections
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                runPeriod: self.config.runPeriod
                toPropagate: ["JES", "JER", "TES", "ESS"]
    """

    return lambda: metcRDFProducer(**kwargs)


class metxyRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(metxyRDFProducer, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year") ; year = str(self.year)
        self.runPeriod = kwargs.pop("runPeriod")

        prefix = "" ; isUL = False
        try:
            isUL = kwargs.pop("isUL")
            prefix += "" if not isUL else "UL"
        except KeyError:
            pass
        try:
            prefix += self.runPeriod
        except KeyError:
            pass

        self.corrKey = prefix+year
        self.LUT = "xy_corr_"+year
        self.epoch = corrCfg["metxy"][self.corrKey]["epoch"]

        if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
            ROOT.gInterpreter.Load("libBaseModules.so")
        base = "{}/src/Base/Modules".format(os.getenv("CMSSW_BASE"))
        if not ROOT.gInterpreter.IsLoaded("{}/interface/correctionWrapper.h".format(base)):
            ROOT.gROOT.ProcessLine(".L {}/interface/correctionWrapper.h".format(base))

        if not os.getenv("_METxy_"+self.LUT):
            os.environ["_METxy_"+self.LUT] = "METxy_"+self.LUT

            ROOT.gInterpreter.ProcessLine(
                'auto %s = MyCorrections("%s", "%s");' %
                    (self.LUT, corrCfg["metxy"][self.corrKey]["fileName"],
                        corrCfg["metxy"][self.corrKey]["corrName"]))

    def run(self, df):
        branches = []

        if self.isMC:
            df = df.Define('PuppiMET_smeared_xycorr_pt', 
                           '%s.eval({"pt", "PuppiMET", "%s", "MC", "nom", '
                                     "PuppiMET_smeared_pt, PuppiMET_smeared_phi, "
                                     "(float)PV_npvsGood})" % (self.LUT, self.epoch))

            df = df.Define('PuppiMET_smeared_xycorr_phi', 
                           '%s.eval({"phi", "PuppiMET", "%s", "MC", "nom", '
                                     "PuppiMET_smeared_pt, PuppiMET_smeared_phi, "
                                     "(float)PV_npvsGood})" % (self.LUT, self.epoch))

        else:
            df = df.Define('PuppiMET_smeared_xycorr_pt', 
                           '%s.eval({"pt", "PuppiMET", "%s", "DATA", "nom", '
                                     "PuppiMET_smeared_pt, PuppiMET_smeared_phi, "
                                     "(float)PV_npvsGood})" % (self.LUT, self.epoch))

            df = df.Define('PuppiMET_smeared_xycorr_phi', 
                           '%s.eval({"phi", "PuppiMET", "%s", "DATA", "nom", '
                                     "PuppiMET_smeared_pt, PuppiMET_smeared_phi, "
                                     "(float)PV_npvsGood})" % (self.LUT, self.epoch))

        branches.append("PuppiMET_smeared_xycorr_pt")
        branches.append("PuppiMET_smeared_xycorr_phi")

        return df, branches

def metxyRDF(**kwargs):
    """
    Module to apply the xy correction to the MET.

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: metcRDF
            path: Base.Modules.jmeCorrections
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                runPeriod: self.config.runPeriod

    """

    return lambda: metxyRDFProducer(**kwargs)
