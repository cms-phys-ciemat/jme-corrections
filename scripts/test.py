import os
import ROOT
ROOT.gROOT.SetBatch(True)

from Corrections.JME.jetCorrections import jecRDF, metcRDF, metxyRDF
from Corrections.JME.smearing import jetSmearerRDF, jetVarRDF
test_folder = "$CMSSW_BASE/src/Base/Modules/data/"
get_test_file = lambda x: os.path.expandvars(os.path.join(test_folder, x))

import pandas as pd

import argparse
parser = argparse.ArgumentParser(description='Plotter options')
parser.add_argument('-s','--save', action='store_true', default=False,
    help="Stores test results in a pickle file")
parser.add_argument('-c','--check', action='store_true', default=False,
    help="Compares test results with previously stored results")
options = parser.parse_args()

def jec_test(df, isMC, year, runPeriod, runEra, jectype):
    jec = jecRDF(
        isMC=isMC,
        year=year,
        runPeriod=runPeriod,
        runEra=runEra,
        type=jectype
    )()
    df, _ = jec.run(df)
    var = "jec_sf"
    if jectype == 'fatjerc': var = "fatjec_sf" 
    h = df.Histo1D(var)
    print(f"Jet {year}{runPeriod} Era{runEra} {var} Integral: %.3f, Mean: %.3f, Std: %.3f" % 
            (h.Integral(), h.GetMean(), h.GetStdDev()))
    return df, (h.Integral(), h.GetMean(), h.GetStdDev())

def jer_test(df, isMC, year, runPeriod, runEra, jectype, after_jec):
    jer = jetSmearerRDF(
        isMC=isMC,
        year=year,
        runPeriod=runPeriod,
        runEra=runEra,
        type=jectype,
        after_jec=after_jec
    )()
    df, _ = jer.run(df)
    jervar = jetVarRDF(
        isMC=isMC,
        year=year,
        type=jectype,
        after_jec=after_jec
    )()
    df, _ = jervar.run(df)
    var = "jet_smear_factor"
    if jectype == 'fatjerc': var = "fatjet_smear_factor" 
    h = df.Histo1D(var)
    print(f"Jet {year}{runPeriod} Era{runEra} {var} (after_jec={after_jec}) %.3f, Mean: %.3f, Std: %.3f" % 
            (h.Integral(), h.GetMean(), h.GetStdDev()))

    return df, (h.Integral(), h.GetMean(), h.GetStdDev())

def metc_test(df, isMC, year, runPeriod, runEra, toPropagate):
    metc = metcRDF(
        isMC=isMC,
        year=year,
        toPropagate=toPropagate,
    )()
    df, _ = metc.run(df)
    var = "PuppiMET_smeared_pt"
    h = df.Histo1D(var)
    print(f"PuppiMET_smeared_pt {year}{runPeriod} Era{runEra} {var} %.3f, Mean: %.3f, Std: %.3f" % 
            (h.Integral(), h.GetMean(), h.GetStdDev()))

    return df, (h.Integral(), h.GetMean(), h.GetStdDev())

def metxy_test(df, isMC, year, runPeriod, runEra):
    metxy = metxyRDF(
        isMC=isMC,
        year=year,
        runPeriod=runPeriod
    )()
    df, _ = metxy.run(df)
    var = "PuppiMET_smeared_xycorr_pt"
    h = df.Histo1D(var)
    print(f"PuppiMET_smeared_xycorr_pt {year}{runPeriod} Era{runEra} {var} %.3f, Mean: %.3f, Std: %.3f" % 
            (h.Integral(), h.GetMean(), h.GetStdDev()))

    return df, (h.Integral(), h.GetMean(), h.GetStdDev())



if __name__ == "__main__":
    results = []

    df_mc2022 = ROOT.RDataFrame("Events", get_test_file("testfile_mc2022.root"))
    df_mc2022, res = jec_test(df_mc2022, True, 2022, "preEE", "_", "jerc")
    results.append(("2022_jec", res[0], res[1], res[2]))
    _, res = jer_test(df_mc2022, True, 2022, "preEE", "_", "jerc", True)
    results.append(("2022_jer_afterjec", res[0], res[1], res[2]))
    _, res = metc_test(_, True, 2022, "preEE", "_", ["JES", "JER"])
    results.append(("2022_metc", res[0], res[1], res[2]))
    _, res = metxy_test(_, True, 2022, "preEE", "_")
    results.append(("2022_metxy", res[0], res[1], res[2]))
    _, res = jer_test(df_mc2022, True, 2022, "preEE", "_", "jerc", False)
    results.append(("2022_jer", res[0], res[1], res[2]))
    df_mc2022, res = jec_test(df_mc2022, True, 2022, "preEE", "_", "fatjerc")
    results.append(("2022_fatjerc_jec", res[0], res[1], res[2]))
    _, res = jer_test(df_mc2022, True, 2022, "preEE", "_", "fatjerc", True)
    results.append(("2022_fatjerc_jer_afterjec", res[0], res[1], res[2]))
    _, res = jer_test(df_mc2022, True, 2022, "preEE", "_", "fatjerc", False)
    results.append(("2022_fatjerc_afterjec", res[0], res[1], res[2]))

    df_mc2022_postEE = ROOT.RDataFrame("Events", get_test_file("testfile_mc2022_postee.root"))
    df_mc2022_postEE, res = jec_test(df_mc2022_postEE, True, 2022, "postEE", "_", "jerc")
    results.append(("2022_postEE_jec", res[0], res[1], res[2]))
    _, res = jer_test(df_mc2022_postEE, True, 2022, "postEE", "_", "jerc", True)
    results.append(("2022_postEE_jer_afterjec", res[0], res[1], res[2]))
    _, res = metc_test(_, True, 2022, "postEE", "_", ["JES", "JER"])
    results.append(("2022_postEE_metc", res[0], res[1], res[2]))
    _, res = metxy_test(_, True, 2022, "postEE", "_")
    results.append(("2022_postEE_metxy", res[0], res[1], res[2]))
    _, res = jer_test(df_mc2022_postEE, True, 2022, "postEE", "_", "jerc", False)
    results.append(("2022_postEE_jer", res[0], res[1], res[2]))
    df_mc2022_postEE, res = jec_test(df_mc2022_postEE, True, 2022, "postEE", "_", "fatjerc")
    results.append(("2022_postEE_fatjerc_jec", res[0], res[1], res[2]))
    _, res = jer_test(df_mc2022_postEE, True, 2022, "postEE", "_", "fatjerc", True)
    results.append(("2022_postEE_fatjerc_jer_afterjec", res[0], res[1], res[2]))
    _, res = jer_test(df_mc2022_postEE, True, 2022, "postEE", "_", "fatjerc", False)
    results.append(("2022_postEE_fatjerc_jer", res[0], res[1], res[2]))

    df_data2022 = ROOT.RDataFrame("Events", get_test_file("testfile_data2022.root"))
    _, res = jec_test(df_data2022, False, 2022, "preEE", "C", "jerc")
    results.append(("2022_data_jec", res[0], res[1], res[2]))
    _, res = metc_test(_, False, 2022, "preEE", "_", ["JES", "JER"])
    results.append(("2022_data_metc", res[0], res[1], res[2]))
    _, res = metxy_test(_, False, 2022, "preEE", "_")
    results.append(("2022_data_metxy", res[0], res[1], res[2]))
    _, res = jec_test(df_data2022, False, 2022, "preEE", "C", "fatjerc")
    results.append(("2022_data_fatjerc_jec", res[0], res[1], res[2]))

    df_data2022_postEE = ROOT.RDataFrame("Events", get_test_file("testfile_data2022_postee.root"))
    _, res = jec_test(df_data2022_postEE, False, 2022, "postEE", "E", "jerc")
    results.append(("2022_postEE_data_jec", res[0], res[1], res[2]))
    _, res = metc_test(_, False, 2022, "postEE", "_", ["JES", "JER"])
    results.append(("2022_postEE_data_metc", res[0], res[1], res[2]))
    _, res = metxy_test(_, False, 2022, "postEE", "_")
    results.append(("2022_postEE_data_metxy", res[0], res[1], res[2]))
    _, res = jec_test(df_data2022_postEE, False, 2022, "postEE", "E", "fatjerc")
    results.append(("2022_postEE_data_fatjerc_jec", res[0], res[1], res[2]))

    df_mc2023 = ROOT.RDataFrame("Events", get_test_file("testfile_mc2023.root"))
    df_mc2023, res = jec_test(df_mc2023, True, 2023, "preBPix", "_", "jerc")
    results.append(("2023_jec", res[0], res[1], res[2]))
    _, res = jer_test(df_mc2023, True, 2023, "preBPix", "_", "jerc", True)
    results.append(("2023_jer_afterjec", res[0], res[1], res[2]))
    _, res = metc_test(_, True, 2023, "preBPix", "_", ["JES", "JER"])
    results.append(("2023_metc", res[0], res[1], res[2]))
    _, res = metxy_test(_, True, 2023, "preBPix", "_")
    results.append(("2023_metxy", res[0], res[1], res[2]))
    _, res = jer_test(df_mc2023, True, 2023, "preBPix", "_", "jerc", False)
    results.append(("2023_jer", res[0], res[1], res[2]))
    df_mc2023, res = jec_test(df_mc2023, True, 2023, "preBPix", "_", "fatjerc")
    results.append(("2023_fatjerc_jec", res[0], res[1], res[2]))
    _, res = jer_test(df_mc2023, True, 2023, "preBPix", "_", "fatjerc", True)
    results.append(("2023_fatjerc_jer_afterjec", res[0], res[1], res[2]))
    _, res = jer_test(df_mc2023, True, 2023, "preBPix", "_", "fatjerc", False)
    results.append(("2023_fatjerc_jer", res[0], res[1], res[2]))

    df_mc2023_postBpix = ROOT.RDataFrame("Events", get_test_file("testfile_mc2023_postbpix.root"))
    df_mc2023_postBpix, res = jec_test(df_mc2023_postBpix, True, 2023, "postBPix", "_", "jerc")
    results.append(("2023_postBPix_jec", res[0], res[1], res[2]))
    _, res = jer_test(df_mc2023_postBpix, True, 2023, "postBPix", "_", "jerc", True)
    results.append(("2023_postBPix_jer_afterjec", res[0], res[1], res[2]))
    _, res = jer_test(df_mc2023_postBpix, True, 2023, "postBPix", "_", "jerc", False)
    results.append(("2023_postBPix_jer", res[0], res[1], res[2]))
    _, res = metc_test(_, True, 2023, "postBPix", "_", ["JES", "JER"])
    results.append(("2023_postBPix_metc", res[0], res[1], res[2]))
    _, res = metxy_test(_, True, 2023, "postBPix", "_")
    results.append(("2023_postBPix_metxy", res[0], res[1], res[2]))
    df_mc2023_postBpix, res = jec_test(df_mc2023_postBpix, True, 2023, "postBPix", "_", "fatjerc")
    results.append(("2023_postBPix_fatjerc_jec", res[0], res[1], res[2]))
    _, res = jer_test(df_mc2023_postBpix, True, 2023, "postBPix", "_", "fatjerc", True)
    results.append(("2023_postBPix_fatjerc_jer_afterjec", res[0], res[1], res[2]))
    _, res = jer_test(df_mc2023_postBpix, True, 2023, "postBPix", "_", "fatjerc", False)
    results.append(("2023_postBPix_fatjerc_jer", res[0], res[1], res[2]))

    df_data2023 = ROOT.RDataFrame("Events", get_test_file("testfile_data2023.root"))
    _, res = jec_test(df_data2023, False, 2023, "preBPix", "Cv2", "jerc")
    results.append(("2023_data_jec", res[0], res[1], res[2]))
    _, res = metc_test(_, False, 2023, "preBPix", "_", ["JES", "JER"])
    results.append(("2023_data_metc", res[0], res[1], res[2]))
    _, res = metxy_test(_, False, 2023, "preBPix", "_")
    results.append(("2023_data_metxy", res[0], res[1], res[2]))
    _, res = jec_test(df_data2023, False, 2023, "preBPix", "Cv2", "fatjerc")
    results.append(("2023_data_fatjerc_jec", res[0], res[1], res[2]))

    df_data2023_postBpix = ROOT.RDataFrame("Events", get_test_file("testfile_data2023_postbpix.root"))
    _, res = jec_test(df_data2023_postBpix, False, 2023, "postBPix", "D", "jerc")
    results.append(("2023_postBPix_data_jec", res[0], res[1], res[2]))
    _, res = metc_test(_, False, 2023, "postBPix", "_", ["JES", "JER"])
    results.append(("2023_postBPix_data_metc", res[0], res[1], res[2]))
    _, res = metxy_test(_, False, 2023, "postBPix", "_")
    results.append(("2023_postBPix_data_metxy", res[0], res[1], res[2]))
    _, res = jec_test(df_data2023_postBpix, False, 2023, "postBPix", "D", "fatjerc")
    results.append(("2023_postBPix_data_fatjerc_jec", res[0], res[1], res[2]))

    pd_df = pd.DataFrame(results)

    results_path = os.path.expandvars("$CMSSW_BASE/src/Corrections/JME/scripts/results.pickle")
    if options.save:
        pd_df.to_pickle(results_path)
    elif options.check:
        stored_df = pd.read_pickle(results_path)
        pd_matches_with_saved_df = pd_df.equals(stored_df)
        if not pd_matches_with_saved_df:
            raise ValueError("Test results obtained do not agree with the stored results. If this "
                "is expected, please execute locally python3 test.py -s to overwrite the "
                "stored results and upload the resulting file")
